package techproduct;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Filter {

	// filter by brand name

	public static List<TechProduct> filterByBrand(List<TechProduct> techproducts, String brand) {
		List<TechProduct> result = new LinkedList<TechProduct>();
		for (TechProduct techProduct : techproducts) {
			if (techProduct.getBrand().toLowerCase().contains(brand)) {
				result.add(techProduct);
			}

		}

		return result;

	}

	
	
	public static List<TechProduct> filterByModelName(List<TechProduct> techProducts, String modelName) {
	    String lowerCaseModelName = modelName.toLowerCase();

	    return techProducts.stream()
	            .filter(product ->
	                    product.getBrand().toLowerCase().contains(lowerCaseModelName) ||
	                            product.getModel().toLowerCase().contains(lowerCaseModelName) ||
	                            String.valueOf(product.getBatteryLifeHours()).contains(lowerCaseModelName) ||
	                            String.valueOf(product.getProcessorSpeedGHz()).contains(lowerCaseModelName) ||  
	                            String.valueOf(product.getCameraQualityMP()).contains(lowerCaseModelName))
	            .collect(Collectors.toList());
	}
	  
	  
	  
	private static double parseDoubleOrDefault(String value, double defaultValue) {
	    try {
	        return Double.parseDouble(value);
	    } catch (NumberFormatException e) {
	        return defaultValue;
	    }
	}

	public static List<TechProduct> filterWithParallelStream(List<TechProduct> techProducts, String modelName) {
	    String lowerCaseModelName = modelName.toLowerCase();

	    return techProducts.parallelStream()
	            .filter(product ->
	                    product.getBrand().toLowerCase().contains(lowerCaseModelName) ||  // Updated from startsWith to contains
	                            product.getModel().toLowerCase().contains(lowerCaseModelName) ||  // Added condition for model name
	                            String.valueOf(product.getBatteryLifeHours()).contains(lowerCaseModelName) ||
	                            Double.compare(product.getProcessorSpeedGHz(), parseDoubleOrDefault(lowerCaseModelName, Double.NaN)) == 0 ||
	                            String.valueOf(product.getCameraQualityMP()).contains(lowerCaseModelName))
	            .collect(Collectors.toList());
	}

	public static List<TechProduct> filterWithSequentialStream(List<TechProduct> techProducts, String modelName) {
	    String lowerCaseModelName = modelName.toLowerCase();

	    return techProducts.stream()
	            .filter(product ->
	                    product.getBrand().toLowerCase().contains(lowerCaseModelName) ||  // Updated from startsWith to contains
	                            product.getModel().toLowerCase().contains(lowerCaseModelName) ||  // Added condition for model name
	                            String.valueOf(product.getBatteryLifeHours()).contains(lowerCaseModelName) ||
	                            Double.compare(product.getProcessorSpeedGHz(), parseDoubleOrDefault(lowerCaseModelName, Double.NaN)) == 0 ||
	                            String.valueOf(product.getCameraQualityMP()).contains(lowerCaseModelName))
	            .collect(Collectors.toList());
	}
	
	
	//using Predicate  for filtering based on various attributes
	

	public static List<TechProduct> filter(List<TechProduct> techProducts, String filterValue, boolean useParallel) {
        Predicate<TechProduct> filterPredicate;

        try {
            double numericValue = Double.parseDouble(filterValue);

            // Numeric comparison for battery life, processor speed, camera quality and Storage capacity
            filterPredicate = product ->
                    product.getBrand().toLowerCase().contains(filterValue) ||
                            product.getModel().toLowerCase().contains(filterValue) ||
                            Double.compare(product.getBatteryLifeHours(), numericValue) == 0 ||
                            Double.compare(product.getProcessorSpeedGHz(), numericValue) == 0 ||
                            Double.compare(product.getCameraQualityMP(), numericValue) == 0 ||
                            Double.compare(product.getStorageCapacityGB(), numericValue) == 0;
        } catch (NumberFormatException e) {
            // Handle non-numeric input 
            String lowerCaseFilter = filterValue.toLowerCase();
            filterPredicate = product ->
                    product.getBrand().toLowerCase().contains(lowerCaseFilter) ||
                            product.getModel().toLowerCase().contains(lowerCaseFilter);
        }

        return techProducts.stream()
                .filter(useParallel ? filterPredicate::test : filterPredicate::test)
                .collect(Collectors.toList());
    }

	public static void main(String[] args) {
		
		
		
	}
}