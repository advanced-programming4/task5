package techproduct;

public class TechProduct implements Comparable<TechProduct>{
	private  String brand;
    private String model;
    private int storageCapacityGB;
    private int ramGB;
    private double processorSpeedGHz;
    private int cameraQualityMP;
    private int batteryLifeHours;
    private double displaySizeInches;
    private double weightKg;
    private double priceLKR;

    public TechProduct(String brand, String model, int storageCapacityGB, int ramGB,
                       double processorSpeedGHz, int cameraQualityMP, int batteryLifeHours,
                       double displaySizeInches, double weightKg, double priceLKR) {
        this.brand=brand;
        this.model=model;
        this.storageCapacityGB=storageCapacityGB;
        this.ramGB=ramGB;
        this.processorSpeedGHz=processorSpeedGHz;
        this.cameraQualityMP=cameraQualityMP;
        this.batteryLifeHours=batteryLifeHours;
        this.displaySizeInches=displaySizeInches;
        this.weightKg=weightKg;
        this.priceLKR=priceLKR;
    }
	
	public  String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public int getStorageCapacityGB() {
		return storageCapacityGB;
	}


	public void setStorageCapacityGB(int storageCapacityGB) {
		this.storageCapacityGB = storageCapacityGB;
	}


	public int getRamGB() {
		return ramGB;
	}


	public void setRamGB(int ramGB) {
		this.ramGB = ramGB;
	}


	public double getProcessorSpeedGHz() {
		return processorSpeedGHz;
	}


	public void setProcessorSpeedGHz(double processorSpeedGHz) {
		this.processorSpeedGHz = processorSpeedGHz;
	}


	public int getCameraQualityMP() {
		return cameraQualityMP;
	}


	public void setCameraQualityMP(int cameraQualityMP) {
		this.cameraQualityMP = cameraQualityMP;
	}


	public int getBatteryLifeHours() {
		return batteryLifeHours;
	}


	public void setBatteryLifeHours(int batteryLifeHours) {
		this.batteryLifeHours = batteryLifeHours;
	}


	public double getDisplaySizeInches() {
		return displaySizeInches;
	}


	public void setDisplaySizeInches(double displaySizeInches) {
		this.displaySizeInches = displaySizeInches;
	}


	public double getWeightKg() {
		return weightKg;
	}


	public void setWeightKg(double weightKg) {
		this.weightKg = weightKg;
	}


	public double getPriceLKR() {
		return priceLKR;
	}


	public void setPriceLKR(double priceLKR) {
		this.priceLKR = priceLKR;
	}
	
    public int compareTo(TechProduct other) {
        return Double.compare(priceLKR, other.priceLKR);
        
    }
    public String toString() {
        return String.format("%-15s%-20s%-15d%-10d%-15.2f%-15d%-15d%-20.2f%-10.2f%-10.2f",
                brand, model, storageCapacityGB, ramGB, processorSpeedGHz,
                cameraQualityMP, batteryLifeHours, displaySizeInches, weightKg, priceLKR);
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	

}
