package techproduct;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TechProductGUI {
	private JFrame frame;
    private JTextArea outputArea;

    public TechProductGUI() {
        frame = new JFrame("Tech Product GUI");
        frame.setSize(800, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        
        outputArea = new JTextArea();
        outputArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(outputArea);
        panel.add(scrollPane, BorderLayout.CENTER);

        
        JButton displayButton = new JButton("Display Tech Products");
        displayButton.addActionListener(e -> displayTechProducts());
        panel.add(displayButton, BorderLayout.SOUTH);

        frame.add(panel);
    }

    private void displayTechProducts() {
        List<TechProduct> techProducts = createTechProductsList();
        StringBuilder result = new StringBuilder();
        result.append("Brand           Model               Storage(GB)   RAM(GB)   Processor(GHz)   " +
                "Camera(MP)   Battery(Hrs)   Display(Inches)   Weight(Kg)   Price(LKR)\n");

        techProducts.forEach(product -> result.append(product.toString()).append("\n"));

        outputArea.setText(result.toString());
    }

    private List<TechProduct> createTechProductsList() {
        List<TechProduct> techProducts = new ArrayList<>();
        techProducts.add(new TechProduct("Apple", "iPhone 13 Pro", 256, 8, 3.2,
		        12, 22, 6.1, 0.18, 249999));
		techProducts.add(new TechProduct("Samsung", "Galaxy Z Fold 3", 512, 12, 2.84,
		        108, 24, 7.6, 0.28, 359999));
		techProducts.add(new TechProduct("Sony", "PlayStation 5 Pro", 1000, 16, 3.8,
		        0, 0, 0, 5.2, 279999));
		techProducts.add(new TechProduct("Microsoft", "Surface Pro 8", 512, 16, 2.9,
		        0, 15, 12.3, 0.78, 189999));
		techProducts.add(new TechProduct("Google", "Pixel 6 Pro", 256, 12, 3.0,
		        50, 26, 6.7, 0.23, 189999));
		techProducts.add(new TechProduct("Dell", "XPS 15", 1000, 32, 4.0,
		        0, 16, 15.6, 2.0, 289999));
		techProducts.add(new TechProduct("OnePlus", "9T", 128, 8, 2.96,
		        48, 20, 6.5, 0.19, 129999));
		techProducts.add(new TechProduct("Huawei", "MateBook X Pro", 512, 16, 3.5,
		        0, 12, 13.9, 1.33, 239999));
		techProducts.add(new TechProduct("Amazon", "Kindle Oasis", 32, 0, 0,
		        0, 6, 7.0, 0.19, 34999));
		techProducts.add(new TechProduct("Apple", "MacBook Pro 14", 1024, 32, 3.5,
		        0, 16, 14.2, 1.4, 429999));
        

        return techProducts;
    }

    public void show() {
        frame.setVisible(true);
    }
        
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
        TechProductGUI techProductGUI = new TechProductGUI();
        techProductGUI.show();
	});
}
}
