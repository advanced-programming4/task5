package techproduct;

import java.util.ArrayList;
import java.util.List;

public class ProductFilters {

	public static void main(String[] args) {
		List<TechProduct> techProducts = new ArrayList<>();
		techProducts.add(new TechProduct("Apple", "iPhone 13 Pro", 256, 8, 3.2,
				12, 22, 6.1, 0.18, 249999));
		techProducts.add(new TechProduct("Samsung", "Galaxy Z Fold 3", 512, 12, 2.84,
		        108, 24, 7.6, 0.28, 359999));
		techProducts.add(new TechProduct("Sony", "PlayStation 5 Pro", 1000, 16, 3.8,
		        0, 0, 0, 5.2, 279999));
		techProducts.add(new TechProduct("Microsoft", "Surface Pro 8", 512, 16, 2.9,
		        0, 15, 12.3, 0.78, 189999));
		techProducts.add(new TechProduct("Google", "Pixel 6 Pro", 256, 12, 3.0,
		        50, 26, 6.7, 0.23, 189999));
		techProducts.add(new TechProduct("Dell", "XPS 15", 1000, 32, 4.0,
		        0, 16, 15.6, 2.0, 289999));
		techProducts.add(new TechProduct("OnePlus", "9T", 128, 8, 2.96,
		        48, 20, 6.5, 0.19, 129999));
		techProducts.add(new TechProduct("Huawei", "MateBook X Pro", 512, 16, 3.5,
		        0, 12, 13.9, 1.33, 239999));
		techProducts.add(new TechProduct("Amazon", "Kindle Oasis", 32, 0, 0,
		        0, 6, 7.0, 0.19, 34999));
		techProducts.add(new TechProduct("Apple", "MacBook Pro 14", 1024, 32, 3.5,
		        0, 16, 14.2, 1.4, 429999));
		
		System.out.println("Brand           Model               Storage(GB)   RAM(GB)   Processor(GHz)   " +
		        "Camera(MP)   Battery(Hrs)   Display(Inches)   Weight(Kg)   Price(LKR)");
		// applying filters
		
		
				//Tech Products with 256 GB Storage
				
				System.out.println("\nTech Products with 256 GB Storage");
				techProducts.stream().filter(product -> product.getStorageCapacityGB() == 256)
				        .forEach(System.out::println);
				
				//Tech Products with 16 GB RAM
				System.out.println("\nTech Products with 16 GB RAM");
				techProducts.stream().filter(product -> product.getRamGB() == 16)
				        .forEach(System.out::println);
				
				//Tech Products with Processor Speed Greater Than 3.0 GHz
				System.out.println("\nTech Products with Processor Speed Greater Than 3.0 GHz");
				techProducts.stream().filter(product -> product.getProcessorSpeedGHz() > 3.0)
				        .forEach(System.out::println);
				
				//Tech Products with Price Less Than 150,000 LKR
				System.out.println("\nTech Products with Price Less Than 150,000 LKR");
				techProducts.stream().filter(product -> product.getPriceLKR() < 150000)
				        .forEach(System.out::println);
				
				//Tech Products with Battery Life Over 20 Hours
				System.out.println("\nTech Products with Battery Life Over 20 Hours");
				techProducts.stream().filter(product -> product.getBatteryLifeHours() > 20)
				        .forEach(System.out::println);
	
				//Tech products with 256GB storage and 12GB RAM
				System.out.println("\nTech Products with 256 GB Storage and 12 GB RAM");
				techProducts.stream()
				        .filter(product -> product.getStorageCapacityGB() == 256 && product.getRamGB() == 12)
				        .forEach(System.out::println);
				
				// Same filter using Lambda
				System.out.println("\nTech Products with 256 GB Storage and 12 GB RAM (Using Lambda)");
				techProducts.stream()
				        .filter(product -> {
				            int requiredStorage = 256;
				            int requiredRam = 12;
				            return product.getStorageCapacityGB() == requiredStorage && product.getRamGB() == requiredRam;
				        })
				        .forEach(System.out::println);
	}

}
