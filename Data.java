package techproduct;

import java.util.*;

public class Data {

	public static void main(String[] args) {
		List<TechProduct> techProducts = new ArrayList<>();
		techProducts.add(new TechProduct("Apple", "iPhone 13 Pro", 256, 8, 3.2,
		        12, 22, 6.1, 0.18, 249999));
		techProducts.add(new TechProduct("Samsung", "Galaxy Z Fold 3", 512, 12, 2.84,
		        108, 24, 7.6, 0.28, 359999));
		techProducts.add(new TechProduct("Sony", "PlayStation 5 Pro", 1000, 16, 3.8,
		        0, 0, 0, 5.2, 279999));
		techProducts.add(new TechProduct("Microsoft", "Surface Pro 8", 512, 16, 2.9,
		        0, 15, 12.3, 0.78, 189999));
		techProducts.add(new TechProduct("Google", "Pixel 6 Pro", 256, 12, 3.0,
		        50, 26, 6.7, 0.23, 189999));
		techProducts.add(new TechProduct("Dell", "XPS 15", 1000, 32, 4.0,
		        0, 16, 15.6, 2.0, 289999));
		techProducts.add(new TechProduct("OnePlus", "9T", 128, 8, 2.96,
		        48, 20, 6.5, 0.19, 129999));
		techProducts.add(new TechProduct("Huawei", "MateBook X Pro", 512, 16, 3.5,
		        0, 12, 13.9, 1.33, 239999));
		techProducts.add(new TechProduct("Amazon", "Kindle Oasis", 32, 0, 0,
		        0, 6, 7.0, 0.19, 34999));
		techProducts.add(new TechProduct("Apple", "MacBook Pro 14", 1024, 32, 3.5,
		        0, 16, 14.2, 1.4, 429999));
		
		

      
		
		System.out.println("Brand           Model               Storage(GB)   RAM(GB)   Processor(GHz)   " +
		        "Camera(MP)   Battery(Hrs)   Display(Inches)   Weight(Kg)   Price(LKR)");
		
		// Print the list as it is 
		
				techProducts.forEach(System.out::println);
				
		// parallel
				
		System.out.println("\nPrint All Tech Products in Parallel");
		techProducts.parallelStream().forEach(System.out::println);
		
		// sequential
		
		System.out.println("\nPrint All Tech Products Sequentially");
		techProducts.stream().forEach(System.out::println);
		
		
		// different methods of printing the list
		
		System.out.println("\nDifferent methods of printing the list.");
		
		System.out.println("\nSorted by Natural Ordering (Based on Price)");
		techProducts.stream().sorted().forEach(System.out::println);
		
		
		System.out.println("\nSorted by Custom Comparator (Based on Price in Descending Order)");
		techProducts.stream()
		        .sorted((p1, p2) -> Double.compare(p2.getPriceLKR(), p1.getPriceLKR()))
		        .forEach(System.out::println);
		
		System.out.println("\nSorted by Custom Comparator (Based on Display Size)");
		techProducts.stream()
		        .sorted((p1, p2) -> Double.compare(p1.getDisplaySizeInches(), p2.getDisplaySizeInches()))
		        .forEach(System.out::println);
		
		System.out.println("\n\n compairing parallel and sequential.");
		
		
		System.out.println("\n\n");
		
		
		  // Parallel Stream: Print All Tech Products
        long startTimeParallel = System.currentTimeMillis();
        techProducts.parallelStream().forEach(System.out::println);
        long endTimeParallel = System.currentTimeMillis();
        System.out.println("\nTime taken to print all Tech Products in parallel: " + (endTimeParallel - startTimeParallel) + " ms");


        // Sequential Stream: Print All Tech Products
        long startTimeSequential = System.currentTimeMillis();
        techProducts.stream().forEach(System.out::println);
        long endTimeSequential = System.currentTimeMillis();
        System.out.println("\nTime taken to print all Tech Products sequentially: " + (endTimeSequential - startTimeSequential) + " ms");
    
	}

}
