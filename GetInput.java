package techproduct;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GetInput {

	
	private static String choice;
	
	public static void main(String[] args) {
		
		try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter your choice");
            choice = scanner.nextLine();

            if (choice != null) {
            	
            	 List<TechProduct> techProducts = new ArrayList<>();
     			techProducts.add(new TechProduct("Apple", "iPhone 13 Pro", 256, 8, 3.2,12, 22, 6.1, 0.18, 249999));
     			techProducts.add(new TechProduct("Samsung", "Galaxy Z Fold 3", 512, 12, 2.84,108, 24, 7.6, 0.28, 359999));
     			techProducts.add(new TechProduct("Sony", "PlayStation 5 Pro", 1000, 16, 3.8,0, 0, 0, 5.2, 279999));
     			techProducts.add(new TechProduct("Microsoft", "Surface Pro 8", 512, 16, 2.9,0, 15, 12.3, 0.78, 189999));
     			techProducts.add(new TechProduct("Google", "Pixel 6 Pro", 256, 12, 3.0,50, 26, 6.7, 0.23, 189999));
     			techProducts.add(new TechProduct("Dell", "XPS 15", 1000, 32, 4.0,0, 16, 15.6, 2.0, 289999));
     			techProducts.add(new TechProduct("OnePlus", "9T", 128, 8, 2.96, 48, 20, 6.5, 0.19, 129999));
     			techProducts.add(new TechProduct("Huawei", "MateBook X Pro", 512, 16, 3.5,0, 12, 13.9, 1.33, 239999));
     			techProducts.add(new TechProduct("Amazon", "Kindle Oasis", 32, 0, 0,0, 6, 7.0, 0.19, 34999));
     			techProducts.add(new TechProduct("Apple", "MacBook Pro 14", 1024, 32, 3.5, 0, 16, 14.2, 1.4, 429999));
     			
     	        List<TechProduct> techProductsByChoice = Filter.filterByModelName(techProducts, choice);
     	        List<TechProduct> techProductsByChoiceParallel = Filter.filterWithParallelStream(techProducts, choice);
     	        List<TechProduct> techProductsByChoiceSequential = Filter.filterWithSequentialStream(techProducts, choice);
     	        List<TechProduct> filteredProductsByChoiceParallel = Filter.filter(techProducts, choice, true);  // Use parallel stream
     	       List<TechProduct> filteredProductsByChoiceSequential = Filter.filter(techProducts, choice, false);
     	       
     	        System.out.println("Your choice is: " + techProductsByChoice);
     	        System.out.println("\nParallel Stream choice is: " + techProductsByChoiceParallel);
     	        System.out.println("\nSequential Stream choice is: " + techProductsByChoiceSequential);
     	        System.out.println("\nAll Paralell Stream choice is: " + filteredProductsByChoiceParallel);
     	       System.out.println("\nall Sequential Stream choice is: " + filteredProductsByChoiceSequential);
            }
		} 
	}
	    
		 
}
